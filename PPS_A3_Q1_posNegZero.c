//SALU Dissanayake
//202029
//IS1101 Programming and Problem Solving
//Assignment 03
//Question 01

#include <stdio.h>

int main () {

    int num;

    printf ("Enter number: ");
    scanf ("%d",&num);

    if(num>0) {
        printf ("\nIt is a positive number.\n");
    } else if(num<0) {
        printf ("\nIt is a negative number.\n");
    } else {
        printf ("\nIt is zero.\n");
    }

    return 0;

}
