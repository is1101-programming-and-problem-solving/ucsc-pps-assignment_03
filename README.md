# UCSC PPS Assignment_03

1. [Write a C program that takes a number from the user and checks whether that number is either positive or negative or zero.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_03/-/blob/master/PPS_A3_Q1_posNegZero.c)

2. [Write a C program that takes a number from the user and checks whether that number is odd or even.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_03/-/blob/master/PPS_A3_Q2_oddEven.c)

3. [Write a C program to check a given character is Vowel or Consonant.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_03/-/blob/master/PPS_A3_Q3_vowelConsonant.c)
