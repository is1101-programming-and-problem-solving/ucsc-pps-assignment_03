//SALU Dissanayake
//202029
//IS1101 Programming and Problem Solving
//Assignment 03
//Question 03

#include <stdio.h>

int main () {

    char x;

    printf ("Enter character: ");
    scanf ("%c",&x);

    if(x=='A'||x=='E'||x=='I'||x=='O'||x=='U'||x=='a'||x=='e'||x=='i'||x=='o'||x=='u') {
        printf ("\nIt is a vowel.\n");
    } else {
        printf ("\nIt is a consonant.\n");
    }

    return 0;

}

